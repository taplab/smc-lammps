#################################
####    PARAMETERS       ########
#################################
#temperature of the system
variable Temp equal 1.0

#damping parameter, it determines how rapidly the temperature is relaxed
variable tdamp equal 1.0

#persistence length (K of angle coeff)
variable lp equal 3.0

#Length of the run
variable run1 equal 500000000

#Restart every these timesteps
variable trestart equal 50000000

#Dump data every these timesteps
variable dumpfreq equal 10000000

#Dump trajectory every these timesteps
variable dumptraj equal 100000

variable seedthermo equal 125485
variable seedtopo   equal 79654

################################
####	DEFINTIONS 	    ########
################################
variable M equal 50
variable N equal 1000
variable rho equal 0.05

variable simname index poly.m${M}.n${N}.rho${rho}.lp${lp}.linear
variable folder index ../dumps/
shell    mkdir ${folder}
restart ${trestart} ${folder}/Restart.${simname}

############################
####   SIM PARAMETERS    ###
############################
#file with the random seed variables
include parameters.dat

units lj
atom_style angle
boundary   p p p
neighbor 1.2 bin
neigh_modify every 1 delay 1 check yes

read_data ../initfiles/melt.dat extra/bond/per/atom 2

####################################
####    PAIR INTERACTIONS    #######
####################################
pair_style   lj/cut 1.12246
pair_modify  shift yes
pair_coeff   * * 1.0 1.0 1.12246 # purely repulsive

####################################
####    FENE BONDS           #######
####################################
bond_style hybrid fene harmonic
special_bonds fene
bond_coeff   1 fene  30.0   1.5  1.0  1.12246 # K (scaling), R_0 (maximum extent), E_0, r_0 (LJ potential minimum)
bond_coeff   2 harmonic 100 1.1 # K, r_0 = equilibrium length, which is quite small
bond_coeff   3 fene  10.0  1.7 1.0  1.12246

####################################
####	ANGLE	             #######
####################################
angle_style   cosine
angle_coeff  * ${lp} #20 sigma for realistic DNA (1 bead = 2.5 nm)


######################
#### CUSTOM INFO #####
######################
thermo 10000
thermo_style   custom step  temp  epair ebond eangle vol cpu


#########################
#### SMC PARAMETERS #####
#########################
variable prob equal 1
variable lpol equal 1000
variable cutoff equal 1.5
variable rate equal 10000
variable nsmc equal 5000

####################################
####	FIXES	             #######
####################################
timestep 0.01
fix 1 all nve  
fix 2 all langevin   ${Temp} ${Temp} ${tdamp}  ${seedthermo}
fix smcrun all smc ${rate} 213312 ${prob} ${lpol} linear -1 1 ${nsmc} 2 2 2 ${cutoff} distributed 1 0.00001
fix smcdump all dumpsmc 1000 ${nsmc} smcpos smcrun

#Useful commands when running in parallel
#comm_style tiled
#fix bal all balance 10000 1.1 rcb
comm_modify cutoff 3.5


####################################
####    DUMP POSITIONS OF ATOMS ####
####################################
##DAT (best for postprocessing)
dump 1 all custom ${dumpfreq} ${folder}/${simname}.* id type x y z ix iy iz
dump_modify 1 sort id

##CUSTOM (best for visualisation of several molecules)
dump 2 all custom ${dumptraj} ${folder}/test_${simname}_prob${prob}_rate${rate}_cutoff${cutoff}_nsmc${nsmc}_dump_parallel.lammpstrj id mol type x y z ix iy iz
dump_modify 2 sort id
#dump_modify 2 format line "%d %d %d %.6le %.6le %.6le %d %d %d"

compute 1 all property/local batom1 batom2 btype
dump 3 all local ${dumptraj} ${folder}/test_${simname}_prob${prob}_rate${rate}_cutoff${cutoff}_nsmc${nsmc}_bond_parallel.lammpstrj index c_1[1] c_1[2] c_1[3]

run ${run1}

